<?php

namespace TCG\Voyager\Models;

use Carbon\Carbon;
use Illuminate\Foundation\Auth\User as AuthUser;
use Illuminate\Support\Facades\DB;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Traits\VoyagerUser;

class User extends AuthUser
{
    use VoyagerUser;

    protected $guarded = [];

    /**
     * On save make sure to set the default avatar if image is not set.
     */
    public function save(array $options = [])
    {
        // If no avatar has been set, set it to the default
        $this->avatar = $this->avatar ?: config('voyager.user.default_avatar', 'users/default.png');

        parent::save();
    }

    public function setCreatedAtAttribute($value)
    {
        $this->attributes['created_at'] = Carbon::parse($value)->format('Y-m-d H:i:s');
    }
    
    public function roleId()
    {
    	return $this->hasOne(Voyager::modelClass('User'), 'role_id');
    }
    
    public function playlists()
    {
    	return $this->hasMany('App\Playlist', 'user_id');
    }

    public function hasPermissionCustom($role_id, $table, $action) {
    	return DB::table('permission_role as pr')
		    ->join('permissions as p', 'p.id', '=', 'pr.permission_id')
		    ->where('role_id', '=', $role_id)
		    ->where('p.table_name', '=', $table)
		    ->where('key', 'LIKE', '%'.$action.'%')
		    ->selectRaw('COUNT(p.id) as permission')
		    ->first();
    }
    
}
