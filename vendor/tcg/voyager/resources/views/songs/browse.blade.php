@extends('voyager::master')

@section('page_header')
    <h1 class="page-title">
        <i class="voyager-music"></i> Bài hát
        @if($listPer['add'] > 0)
        <a href="{{ route('voyager.songs.create') }}" class="btn btn-success">
            <i class="voyager-plus"></i> Add New
        </a>
        @endif
    </h1>
@stop

@section('content')
    <div class="page-content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <table id="dataTable" class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Tên bài hát</th>
                                    <th>Hình ảnh</th>
                                    <th>Lượt nge</th>
                                    <th>Ca sĩ thể hiện</th>
                                    <th>Tác giả</th>
                                    <th>Tình trạng</th>
                                    <th class="actions">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($objSong as $data)
                                <tr>
                                    <td>{{$data->song_name}}</td>
                                    <td>
                                        @if($data->picture != '')
                                            <img src="{{ asset('storage\\') }}{{ $data->picture }}" style="width: 100px" alt="">
                                        @else
                                            Đang cập nhật
                                        @endif
                                    </td>
                                    <td>{{ $data->count_listen }}</td>
                                    <td>
                                        @if($data->artist_name == '')
                                            @php
                                            $objArts = $data->artists($data->id);
                                            @endphp
                                            @foreach($objArts as $key => $artist)
                                                @if(!empty($objArts[$key+1]))
                                                {{$artist->art_name}} ft.
                                                @else
                                                {{$artist->art_name}}
                                                @endif
                                            @endforeach
                                        @else
                                            {{ $data->artist_name }}
                                        @endif
                                    </td>
                                    <td>
                                        @if($data->author != '')
                                            {{ $data->author }}
                                        @else
                                            Đang cập nhật
                                        @endif
                                    </td>
                                    <td>@if($data->active_song == 1) Đã duyệt @else Chưa duyệt @endif </td>
                                    <td class="no-sort no-click">
                                        @if($listPer['delete'] > 0)
                                            <div class="btn-sm btn-danger pull-right delete" data-id="{{ $data->id }}" id="delete-{{ $data->id }}">
                                                <i class="voyager-trash"></i> Delete
                                            </div>
                                        @endif
                                            @if($listPer['edit'] > 0)
                                            <a href="javascript:void(0)" class="btn-sm btn-primary pull-right edit">
                                                <i class="voyager-edit"></i> Edit
                                            </a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal modal-danger fade" tabindex="-1" id="delete_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><i class="voyager-trash"></i> Are you sure you want to delete
                        this songs?</h4>
                </div>
                <div class="modal-footer">
                    <form action="{{ route('voyager.songs.index') }}" id="delete_form" method="POST">
                        {{ method_field("DELETE") }}
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-danger pull-right delete-confirm"
                                 value="Yes, Delete This songs">
                    </form>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Cancel</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@stop

@section('javascript')
    <!-- DataTables -->
    <script>
        $(document).ready(function () {
            $('#dataTable').DataTable({ "order": [] });
        });

        $('td').on('click', '.delete', function (e) {
            var form = $('#delete_form')[0];

            form.action = parseActionUrl(form.action, $(this).data('id'));

            $('#delete_modal').modal('show');
        });

        function parseActionUrl(action, id) {
            return action.match(/\/[0-9]+$/)
                    ? action.replace(/([0-9]+$)/, id)
                    : action + '/' + id;
        }
    </script>
@stop
