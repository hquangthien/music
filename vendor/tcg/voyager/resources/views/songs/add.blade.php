@extends('voyager::master')

@section('page_header')
    <h1 class="page-title">
        Thêm bài hát
    </h1>
@stop

@section('content')
    <div class="page-content container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered">

                    <div class="panel-heading">
                        <h3 class="panel-title">Thêm mới bài hát</h3>
                    </div>
                    @if(session('msg'))
                        {!! session('msg') !!}
                    @endif
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form class="form-edit-add" role="form"
                          action="{{ route('voyager.songs.store') }}"
                          method="POST" enctype="multipart/form-data">
                        <!-- CSRF TOKEN -->
                        {{ csrf_field() }}

                        <div class="panel-body">
                            <div class="form-group">
                                <label for="name">Tên bài hát</label>
                                <input type="text" class="form-control" name="song_name"
                                    placeholder="Tên bài hát" id="song_name"
                                    value="{{old('song_name')}}">
                            </div>

                            <div class="form-group">
                                <label for="password">Hình ảnh</label>
                                <input type="file" name="picture" class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="name">Ca sĩ thể hiện</label>
                                <select class="form-control select2" name="artist_name[]" multiple>
                                    <option value="">Chọn ca sĩ</option>
                                    @foreach($objArtist as $artist)
                                    <option value="{{ $artist->id }}">{{ $artist->art_name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="author">Tên tác giả</label>
                                <input type="text" class="form-control" name="author"
                                       placeholder="Tên tác giả" id="author"
                                       value="{{old('author')}}">
                            </div>

                            <div class="form-group">
                                <label for="category_id">Thể loại</label>
                                <select class="form-control select2" name="category_id" multiple>
                                    <option value="">Chọn thể loại</option>
                                    @foreach($objCat as $cat)
                                        <option value="{{ $cat->id }}">{{ $cat->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            
                            <div class="form-group">
                                <label for="is_video">Là video?</label>
                                <input type="checkbox" name="is_video" class="toggleswitch">
                            </div>

                            <div class="form-group">
                                <label for="song">Bài hát</label>
                                <input type="file" name="song" class="form-control">
                            </div>

                        </div><!-- panel-body -->

                        <div class="panel-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript')
    <script>
        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();
        });
    </script>
@stop
