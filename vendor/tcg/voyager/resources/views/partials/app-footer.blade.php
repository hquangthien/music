<footer class="app-footer">
    <div class="site-footer-right">
        @if (rand(1,100) == 100)
            <i class="voyager-rum-1"></i> Made with rum and even more rum
        @else
            Made with <i class="voyager-heart"></i> by <a href="javascript:void(0)" target="_blank">My team - KLTN 2017</a>
        @endif
    </div>
</footer>
