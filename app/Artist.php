<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Artist extends Model
{
	protected $table = 'artists';
	public function songs()
	{
		return $this->belongsToMany('App\Songs', 'artist_songs', 'song_id', 'artist_id');
	}
}
