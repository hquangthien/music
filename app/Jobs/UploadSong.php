<?php

namespace App\Jobs;

use App\ArtistSong;
use App\Song;
use App\Source;
use Dropbox\Client;
use Dropbox\WriteMode;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class UploadSong
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $request;
    public function __construct($request)
    {
        $this->request = $request;
    }

    public function handle()
    {
    	$request = $this->request;
	    /*** Xử lý upload file to dropbox ***/
	    $extensionSong = $request->file('song')->getClientOriginalExtension();
	    if (!in_array($extensionSong, ['mp3', 'mp4', 'MP3', 'MP4'])) {
		    return redirect()->route('voyager.songs.create')
			    ->with('msg', '<p class="alert alert-warning">Chỉ được chọn file dạng .mp3 và .mp4</p>');
	    }
	    $objSong = new Song();
	    if ($request->hasFile('picture')) {
		    $picture = $request->file('picture')->store('public/songs');
		    $picture = str_replace('public/', '', $picture);
	    }
	    $objSong->song_name = $request->song_name;
	    $objSong->picture = (!empty($picture))?$picture:'';
	    $objSong->author = $request->author;
	    $objSong->active_song = 1;
	    $objSong->category_id = $request->category_id;
	    $objSong->is_video = ($request->is_video == 'on')?1:0;
	    $objSong->save();
	    foreach ($request->artist_name as $artist) {
		    ArtistSong::create(['artist_id' => $artist, 'song_id' => $objSong->id]);
	    }
	
	    if ($request->is_video == 0) {
		    $quality = '128';
	    } else {
		    $quality = '360';
	    }
	    Source::create([
		    'song_id' => $objSong->id,
		    'source' => $this->uploadFile('song', $extensionSong, $request),
		    'quality' => $quality
	    ]);
    }
	
	public function uploadFile($name, $extensionSong, $request)
	{
		$Client = new Client(env('DROPBOX_TOKEN'), env('DROPBOX_SECRET'));
		
		$file = fopen($request->file(''.$name)->getPathname(), 'rb');
		$size = filesize($request->file(''.$name)->getPathname());
		$dropboxFileName = '/song-'.time().'.'.$extensionSong;
		
		$Client->uploadFile($dropboxFileName,WriteMode::add(),$file, $size);
		/*$links['share'] = $Client->createShareableLink($dropboxFileName);*/
		return $dropboxFileName;
	}
}
