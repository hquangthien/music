<?php

namespace App\Http\Controllers\Admin;

use App\Artist;
use App\Jobs\UploadSong;
use App\Song;
use Closure;
use Dropbox\Client;
use Dropbox\WriteMode;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Models\Category;

class SongController extends Controller
{
	public $listPer;
	public function __construct()
	{
		$this->middleware(function (Request $request, Closure $next) {
			if (!Auth::guest()) {
				$user = Voyager::model('User')->find(Auth::id());
				$action = last(explode('@', Route::currentRouteAction()));
				$this->listPer['add'] = $user->hasPermissionCustom($user->role_id, 'songs', 'add')->permission;
				$this->listPer['edit'] = $user->hasPermissionCustom($user->role_id, 'songs', 'edit')->permission;
				$this->listPer['delete'] = $user->hasPermissionCustom($user->role_id, 'songs', 'delete')->permission;
				switch ($action) {
					case 'index':
						$action = 'browse';
						break;
					case 'update':
						$action = 'edit';
						break;
					case 'destroy':
						$action = 'delete';
						break;
					case 'create':
						$action = 'add';
						break;
					case 'store':
						$action = 'add';
						break;
				}
				$per = $user->hasPermissionCustom($user->role_id, 'songs', $action)->permission;
				if ($per > 0) {
					return $next($request);
				} else {
					Auth::logout();
					return redirect()->route('voyager.login');
				}
			}
		});
	}
	
	public function index()
	{
		$objSong = Song::all();
		return view('voyager::songs.browse', ['objSong' => $objSong, 'listPer' => $this->listPer]);
	}
	
	public function create()
	{
		$objArtist = Artist::all();
		$objCat = Category::all();
		return view('voyager::songs.add', ['objArtist' => $objArtist, 'objCat' => $objCat]);
	}
	
	public function store(Request $request)
	{
		/*** Xử lý upload file to dropbox ***/
		$this->validate($request, [
			'song_name' => 'unique:songs,song_name'
		]);
		$this->dispatch(new UploadSong($request));
		return redirect()->route('voyager.songs.index');
		
	}
	
	public function edit($id)
	{
		$objArtist = Artist::all();
		$objSong = Song::find($id);
		$selectedArt = Artist::join('artist_songs', 'artist_songs.artist_id', '=', 'artists.id')
			->where('song_id', '=', $id)
			->select('artists.*')
			->get();
		$arIdArt = [];
		foreach ($selectedArt as $key => $selectedItem) {
			$arIdArt[$key] = $selectedItem->id;
		}
		return view('voyager::songs.add', [
			'objArtist' => $objArtist,
			'selectedArt' => $selectedArt,
			'objSong' => $objSong,
			'$arIdArt' => $arIdArt,
		]);
	}
	
	public function destroy($id)
	{
		Song::deleteCustom($id);
		return redirect()->route('voyager.songs.index');
	}
	
	public function uploadFile($name, $extensionSong, Request $request)
	{
		$Client = new Client(env('DROPBOX_TOKEN'), env('DROPBOX_SECRET'));
		
		$file = fopen($request->file(''.$name)->getPathname(), 'rb');
		$size = filesize($request->file(''.$name)->getPathname());
		$dropboxFileName = '/song-'.time().'.'.$extensionSong;
		
		$Client->uploadFile($dropboxFileName,WriteMode::add(),$file, $size);
		/*$links['share'] = $Client->createShareableLink($dropboxFileName);*/
		return $Client->createTemporaryDirectLink($dropboxFileName)[0];
	}
}
