<?php

namespace App\Http\Controllers\Nhac;

use App\Song;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    public function index()
    {
    	$objHotMusic = Song::where('is_video', '<>', 1)
		    ->join('sources', 'sources.song_id', '=', 'songs.id')
		    ->orderBy('count_listen', 'DESC')
		    ->orderBy('created_at', 'DESC')
		    ->selectRaw('songs.*, sources.source as source')
	        ->take(5)->get();
    	foreach ($objHotMusic as $key => $items) {
    		$objHotMusic[$key]->source = $items->getSource($items->source);
	    }
	
	    $objNewMusic = Song::where('is_video', '<>', 1)
		    ->join('sources', 'sources.song_id', '=', 'songs.id')
		    ->orderBy('created_at', 'DESC')
		    ->selectRaw('songs.*, sources.source as source')
		    ->get();
	    foreach ($objNewMusic as $key => $items) {
		    $objNewMusic[$key]->source = $items->getSource($items->source);
	    }
	    
    	return view('musicplus.index.index', [
    		'hotMusic' => $objHotMusic,
    		'newMusic' => $objNewMusic,
	    ]);
    }
}
