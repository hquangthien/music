<?php

namespace App\Http\Controllers\Nhac;

use App\Comment;
use App\Song;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use TCG\Voyager\Models\Category;

class MusicController extends Controller
{
	public function cat($slug, $id)
	{
		$selectedCat = Category::findOrFail($id);
		$objCat = Category::all();
		$objMusic = Song::where('is_video', '<>', 1)
			->where('category_id', '=', $id)
			->join('sources', 'sources.song_id', '=', 'songs.id')
			->orderBy('created_at', 'DESC')
			->selectRaw('songs.*, sources.source as source')
			->take(5)->get();
		foreach ($objMusic as $key => $items) {
			$objMusic[$key]->source = $items->getSource($items->source);
		}
		return view('musicplus.music.category', [
			'selectedCat' => $selectedCat,
			'objCat' => $objCat,
			'objMusic' => $objMusic,
		]);
	}
	
	public function catAll()
	{
		$objMusic = Song::where('is_video', '<>', 1)
			->join('sources', 'sources.song_id', '=', 'songs.id')
			->orderBy('created_at', 'DESC')
			->selectRaw('songs.*, sources.source as source')
			->take(5)->get();
		foreach ($objMusic as $key => $items) {
			$objMusic[$key]->source = $items->getSource($items->source);
		}
		$objCat = Category::all();
		return view('musicplus.music.category', [
			'objMusic' => $objMusic,
			'objCat' => $objCat,
		]);
	}
	
	public function detail($slug, $id)
	{
		$objMusic = Song::where('is_video', '<>', 1)
			->where('songs.id', '=', $id)
			->join('sources', 'sources.song_id', '=', 'songs.id')
			->join('categories', 'categories.id',  '=','songs.category_id')
			->join('artist_songs', 'artist_songs.song_id', '=', 'songs.id')
			->selectRaw('songs.*, sources.source as source, artist_songs.artist_id as artist_id, categories.name as cat_name')
			->first();
		$objMusic->count_listen ++;
		$objMusic->save();
		$objMusic->source = $objMusic->getSource($objMusic->source);
		$objRelateMusic = Song::join('sources', 'sources.song_id', '=', 'songs.id')
			->join('artist_songs', 'artist_songs.song_id', '=', 'songs.id')
			->where('is_video', '<>', 1)
			->where('songs.id', '<>', $id)
			->where('artist_songs.artist_id', '=', $objMusic->artist_id)
			->selectRaw('songs.*, sources.source as source')
			->take(5)->get();
		foreach ($objRelateMusic as $key => $items) {
			$objRelateMusic[$key]->source = $items->getSource($items->source);
		}
		
		$objCmt = Comment::where('song_id', '=', $id)->orderBy('created_at', 'DESC')->get();
		/*dd($objCmt);*/
		return view('musicplus.music.detail', [
			'objMusic' => $objMusic,
			'objRelateMusic' => $objRelateMusic,
			'objCmt' => $objCmt,
		]);
	}
	
	public function comment($id, Request $request)
	{
		$objMusic = Song::find($id);
		Comment::create([
			'user_id' => $request->user_id,
			'song_id' => $id,
			'content' => $request->contents,
		]);
		return redirect()->route('nhac.detail', ['slug' => str_slug($objMusic->song_name), 'id' => $objMusic->id]);
	}
}
