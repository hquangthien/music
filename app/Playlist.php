<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Facades\Voyager;


class Playlist extends Model
{
    public function userId()
    {
    	return $this->belongsTo(Voyager::modelClass('User'), 'user_id');
    }
}
