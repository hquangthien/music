<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Facades\Voyager;


class Album extends Model
{
    protected $table = 'albums';
    protected $fillable = ['album_name', 'cat_id', 'picture'];
    
    public function catId()
    {
	    return $this->hasOne(Voyager::modelClass('Category'), 'id', 'cat_id');
    }
}
