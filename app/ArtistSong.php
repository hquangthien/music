<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ArtistSong extends Model
{
    protected $table = 'artist_songs';
    protected $fillable = ['artist_id', 'song_id'];
    public $timestamps = false;
}
