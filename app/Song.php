<?php

namespace App;

use Dropbox\Client;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class Song extends Model
{
    protected $table = 'songs';
    protected $fillable = ['song_name', 'picture', 'count_listen', 'artist_name', 'author', 'active_song', 'is_video'];
    
    public function artists($song_id)
    {
	    return DB::table('songs as s')
		    ->join('artist_songs as a', 'a.song_id', '=', 's.id')
		    ->join('artists', 'artists.id', '=', 'a.artist_id')
		    ->where('song_id', '=', $song_id)
		    ->selectRaw('artists.art_name, artists.id as artist_id')
		    ->get();
    }
    
    public function getSource($name)
    {
	    $Client = new Client('ECWVi8qj0lAAAAAAAAAABtf-UyRkLGezrcGPrhcqOh4EqX7lUH2fG_r-rqM2IwwF',
		    '7jaztus5gdr33c5');
	
	    return $Client->createTemporaryDirectLink('/'.$name)[0];
    }
    
    public static function deleteCustom($id)
    {
	    return DB::table('songs as s')
		    ->join('artist_songs as a', 'a.song_id', '=', 's.id')
		    ->join('sources as so', 'so.song_id', '=', 's.id')
		    ->where('s.id', '=', $id)
		    ->delete();
    }
}
