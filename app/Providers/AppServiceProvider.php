<?php

namespace App\Providers;

use App\Song;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
	
	    $objHotMusic = Song::where('is_video', '<>', 1)
		    ->join('sources', 'sources.song_id', '=', 'songs.id')
		    ->orderBy('count_listen', 'DESC')
		    ->orderBy('created_at', 'DESC')
		    ->selectRaw('songs.*, sources.source as source')
		    ->take(5)->get();
	    foreach ($objHotMusic as $key => $items) {
		    $objHotMusic[$key]->source = $items->getSource($items->source);
	    }
	    
	    View::share('rightBarMusic', $objHotMusic);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
