<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/
Route::pattern('slug', '(.*)');
Route::pattern('id', '([0-9]*)');

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Route::group(['namespace' => 'Nhac', 'middleware' => 'pjax'], function () {
	Route::get('/', 'IndexController@index')->name('nhac.index');
	Route::get('/danh-muc', 'MusicController@catAll')->name('nhac.catAll');
	Route::get('danh-muc/{slug}-{id}', 'MusicController@cat')->name('nhac.cat');
	Route::get('hai-hat/{slug}-{id}.html', 'MusicController@detail')->name('nhac.detail');
	
	Route::post('binh-luan/{id}', 'MusicController@comment')->name('nhac.comment');
});