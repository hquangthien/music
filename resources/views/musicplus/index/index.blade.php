@extends('templates.musicplus.master')
@section('slide')
    <div class="black dk">
        <div class="row no-gutter item-info-overlay">
            @php
                $firstHotMusic = $hotMusic[0];
                unset($hotMusic[0]);
            @endphp
            <div class="col-sm-6 col-xs-6">
                <div class="item r" data-id="item-{{ $firstHotMusic->id }}"
                     data-src="{{ $firstHotMusic->source }}">
                    <div class="item-media ">
                        <a href="{{ route('nhac.detail', ['slug' => str_slug($firstHotMusic->song_name), 'id' => $firstHotMusic->id]) }}" class="item-media-content"
                           style="background-image: url('{{ asset('') }}/images/b0.jpg');"></a>
                        <div class="item-overlay center">
                            <button class="btn-playpause">Play</button>
                        </div>
                    </div>
                    <div class="item-info">
                        <div class="item-overlay bottom text-right">
                            <a href="#" class="btn-favorite"><i class="fa fa-heart-o"></i></a>
                            <a href="#" class="btn-more" data-toggle="dropdown"><i class="fa fa-ellipsis-h"></i></a>
                            <div class="dropdown-menu pull-right black lt"></div>
                        </div>
                        <div class="item-title text-ellipsis">
                            <a href="{{ route('nhac.detail', ['slug' => str_slug($firstHotMusic->song_name), 'id' => $firstHotMusic->id]) }}">{{ $firstHotMusic->song_name }}</a>
                        </div>
                        <div class="item-author text-sm text-ellipsis ">
                            <a href="#" class="text-muted">
                                @if($firstHotMusic->artist_name == '')
                                    @php
                                        $objArts = $firstHotMusic->artists($firstHotMusic->id);
                                    @endphp
                                    @foreach($objArts as $key => $artist)
                                        @if(!empty($objArts[$key+1]))
                                            {{$artist->art_name}} ft.
                                        @else
                                            {{$artist->art_name}}
                                        @endif
                                    @endforeach
                                @else
                                    {{ $firstHotMusic->artist_name }}
                                @endif
                            </a>
                        </div>


                    </div>
                </div>
            </div>
            @foreach($hotMusic as $hotItem)
            <div class="col-sm-3 col-xs-6">
                <div class="item r" data-id="item-{{ $hotItem->id }}"
                     data-src="{{ $hotItem->source }}">
                    <div class="item-media ">
                        <a href="{{ route('nhac.detail', ['slug' => str_slug($hotItem->song_name), 'id' => $hotItem->id]) }}" class="item-media-content"
                           style="background-image: url('{{ asset('') }}/images/b0.jpg');"></a>
                        <div class="item-overlay center">
                            <button class="btn-playpause">Play</button>
                        </div>
                    </div>
                    <div class="item-info">
                        <div class="item-overlay bottom text-right">
                            <a href="#" class="btn-favorite"><i class="fa fa-heart-o"></i></a>
                            <a href="#" class="btn-more" data-toggle="dropdown"><i class="fa fa-ellipsis-h"></i></a>
                            <div class="dropdown-menu pull-right black lt"></div>
                        </div>
                        <div class="item-title text-ellipsis">
                            <a href="{{ route('nhac.detail', ['slug' => str_slug($hotItem->song_name), 'id' => $hotItem->id]) }}">{{ $hotItem->song_name }}</a>
                        </div>
                        <div class="item-author text-sm text-ellipsis ">
                            <a href="#" class="text-muted">
                                @if($hotItem->artist_name == '')
                                    @php
                                        $objArts = $hotItem->artists($hotItem->id);
                                    @endphp
                                    @foreach($objArts as $key => $artist)
                                        @if(!empty($objArts[$key+1]))
                                            {{$artist->art_name}} ft.
                                        @else
                                            {{$artist->art_name}}
                                        @endif
                                    @endforeach
                                @else
                                    {{ $hotItem->artist_name }}
                                @endif
                            </a>
                        </div>


                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
@stop

@section('content')
    <div class="col-lg-9 b-r no-border-md">
        <div class="padding">
            <h2 class="widget-title h4 m-b">Xu hướng</h2>
            <div class="owl-carousel owl-theme owl-dots-center" data-ui-jp="owlCarousel" data-ui-options="{
          margin: 20,
          responsiveClass:true,
          responsive:{
            0:{
              items: 2
            },
              543:{
                  items: 3
              }
          }
        }">
            </div>
            <h2 class="widget-title h4 m-b">Mới nhất</h2>
            <div class="row">
                @foreach($newMusic as $key => $newItem)
                <div class="col-xs-4 col-sm-4 col-md-3">
                    <div class="item r" data-id="item-{{ $newItem->id }}"
                         data-src="{{ $newItem->source }}">
                        <div class="item-media ">
                            <a href="{{ route('nhac.detail', ['slug' => str_slug($newItem->song_name), 'id' => $newItem->id]) }}" class="item-media-content"
                               style="background-image: url('{{ asset('') }}/images/b5.jpg');"></a>
                            <div class="item-overlay center">
                                <button class="btn-playpause">Play</button>
                            </div>
                        </div>
                        <div class="item-info">
                            <div class="item-overlay bottom text-right">
                                <a href="#" class="btn-favorite"><i class="fa fa-heart-o"></i></a>
                                <a href="#" class="btn-more" data-toggle="dropdown"><i class="fa fa-ellipsis-h"></i></a>
                                <div class="dropdown-menu pull-right black lt"></div>
                            </div>
                            <div class="item-title text-ellipsis">
                                <a href="{{ route('nhac.detail', ['slug' => str_slug($newItem->song_name), 'id' => $newItem->id]) }}" class="text-muted">
                                    {{ $newItem->song_name }}
                                </a>
                            </div>
                            <div class="item-author text-sm text-ellipsis ">
                                <a href="javascript:void(0)" class="text-muted">
                                    @if($newItem->artist_name == '')
                                        @php
                                            $objArts = $newItem->artists($newItem->id);
                                        @endphp
                                        @foreach($objArts as $key => $artist)
                                            @if(!empty($objArts[$key+1]))
                                                {{$artist->art_name}} ft.
                                            @else
                                                {{$artist->art_name}}
                                            @endif
                                        @endforeach
                                    @else
                                        {{ $newItem->artist_name }}
                                    @endif
                                </a>
                            </div>


                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            <h2 class="widget-title h4 m-b">Có thể bạn thích</h2>
            <div class="row item-list item-list-md m-b">
                @foreach($hotMusic as $key => $music)
                <div class="col-sm-6">
                    <div class="item r" data-id="item-{{ $music->id }}"
                         data-src="{{ $music->source }}">
                        <div class="item-media ">
                            <a href="{{ route('nhac.detail', ['slug' => str_slug($music->song_name), 'id' => $music->id]) }}" class="item-media-content"
                               style="background-image: url('{{ asset('') }}/images/b3.jpg');"></a>
                            <div class="item-overlay center">
                                <button class="btn-playpause">Play</button>
                            </div>
                        </div>
                        <div class="item-info">
                            <div class="item-overlay bottom text-right">
                                <a href="#" class="btn-favorite"><i class="fa fa-heart-o"></i></a>
                                <a href="#" class="btn-more" data-toggle="dropdown"><i class="fa fa-ellipsis-h"></i></a>
                                <div class="dropdown-menu pull-right black lt"></div>
                            </div>
                            <div class="item-title text-ellipsis">
                                <a href="{{ route('nhac.detail', ['slug' => str_slug($music->song_name), 'id' => $music->id]) }}">
                                    {{ $music->song_name }}
                                </a>
                            </div>
                            <div class="item-author text-sm text-ellipsis ">
                                <a href="#" class="text-muted">
                                    @if($music->artist_name == '')
                                        @php
                                            $objArts = $music->artists($music->id);
                                        @endphp
                                        @foreach($objArts as $key => $artist)
                                            @if(!empty($objArts[$key+1]))
                                                {{$artist->art_name}} ft.
                                            @else
                                                {{$artist->art_name}}
                                            @endif
                                        @endforeach
                                    @else
                                        {{ $music->artist_name }}
                                    @endif
                                </a>
                            </div>


                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>

@stop