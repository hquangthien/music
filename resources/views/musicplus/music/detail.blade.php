@extends('templates.musicplus.master')
@section('slide')
  <div class="padding b-b">
    <div class="row-col">
        <div class="col-sm w w-auto-xs m-b">
          <div class="item w r">
            <div class="item-media">
              <div class="item-media-content" style="background-image: url('{{ asset('')}}/images/b0.jpg');"></div>
            </div>
          </div>
        </div>
        <div class="col-sm">
          <div class="p-l-md no-padding-xs">
            <div class="page-title">
              <h1 class="inline">{{ $objMusic->song_name }}</h1>
            </div>
            <p class="item-desc text-ellipsis text-muted" data-ui-toggle-class="text-ellipsis">
				@if($objMusic->artist_name == '')
					@php
						$objArts = $objMusic->artists($objMusic->id);
					@endphp
					@foreach($objArts as $key => $artist)
						@if(!empty($objArts[$key+1]))
							{{$artist->art_name}} ft.
						@else
							{{$artist->art_name}}
						@endif
					@endforeach
				@else
					{{ $objMusic->artist_name }}
				@endif
			</p>
            <div class="item-action m-b">
              <a class="btn btn-icon white rounded btn-share pull-right" data-toggle="modal" data-target="#share-modal"><i class="fa fa-share-alt"></i></a>
              <button class="btn-playpause text-primary m-r-sm"></button> 
              <span class="text-muted">{{ $objMusic->count_listen }}</span>
              <a class="btn btn-icon rounded btn-favorite"><i class="fa fa-heart-o"></i></a> 
              <span class="text-muted">232</span>
              <div class="inline dropdown m-l-xs">
                <a class="btn btn-icon rounded btn-more" data-toggle="dropdown"><i class="fa fa-ellipsis-h"></i></a>
                <div class="dropdown-menu pull-right black lt"></div>
              </div>
            </div>
            <div class="item-meta">
              <a class="btn btn-xs rounded white">{{ $objMusic->cat_name }}</a>
            </div>
          </div>
        </div>
    </div>
  </div>
@stop
@section('content')
    <div class="col-lg-9 b-r no-border-md">
      <div class="padding">

          <h6 class="m-b">
            <span class="text-muted">by</span> {{ $artist->art_name }}
            <a href="artist.detail.html" data-pjax class="item-author _600">{{ $objMusic->artist_name }}</a>
            <span class="text-muted text-sm">- {{ sizeof($objRelateMusic) + 1 }} bài hát</span>
          </h6>
          <div id="tracks" class="row item-list item-list-xs item-list-li m-b">
                <div class="col-xs-12">
                	<div class="item r" data-id="item-{{ $objMusic->id }}" data-src="{{ $objMusic->source }}">
            			<div class="item-media ">
            				<a href="{{ route('nhac.detail', ['slug' => str_slug($objMusic->song_name), 'id' => $objMusic->id]) }}" class="item-media-content" style="background-image: url('{{ asset('')}}/images/b6.jpg');"></a>
            				<div class="item-overlay center">
            					<button  class="btn-playpause">Play</button>
            				</div>
            			</div>
            			<div class="item-info">
            				<div class="item-overlay bottom text-right">
            					<a href="#" class="btn-favorite"><i class="fa fa-heart-o"></i></a>
            					<a href="#" class="btn-more" data-toggle="dropdown"><i class="fa fa-ellipsis-h"></i></a>
            					<div class="dropdown-menu pull-right black lt"></div>
            				</div>
            				<div class="item-title text-ellipsis">
            					<a href="track.detail.html">{{ $objMusic->song_name }}</a>
            				</div>
            				<div class="item-author text-sm text-ellipsis hide">
            					<a href="#" class="text-muted">
									@if($objMusic->artist_name == '')
										@php
											$objArts = $objMusic->artists($objMusic->id);
										@endphp
										@foreach($objArts as $key => $artist)
											@if(!empty($objArts[$key+1]))
												{{$artist->art_name}} ft.
											@else
												{{$artist->art_name}}
											@endif
										@endforeach
									@else
										{{ $objMusic->artist_name }}
									@endif
								</a>
            				</div>
            				<div class="item-meta text-sm text-muted">
            		          <span class="item-meta-right">5:05</span>
            		        </div>
            
            
            			</div>
            		</div>
            	</div>
			  @foreach($objRelateMusic as $music)
                <div class="col-xs-12">
                	<div class="item r" data-id="item-{{ $music->id }}" data-src="{{ $music->source }}">
            			<div class="item-media ">
            				<a href="{{ route('nhac.detail', ['slug' => str_slug($music->song_name), 'id' => $music->id]) }}" class="item-media-content" style="background-image: url('{{ asset('')}}/images/b8.jpg');"></a>
            				<div class="item-overlay center">
            					<button  class="btn-playpause">Play</button>
            				</div>
            			</div>
            			<div class="item-info">
            				<div class="item-overlay bottom text-right">
            					<a href="#" class="btn-favorite"><i class="fa fa-heart-o"></i></a>
            					<a href="#" class="btn-more" data-toggle="dropdown"><i class="fa fa-ellipsis-h"></i></a>
            					<div class="dropdown-menu pull-right black lt"></div>
            				</div>
            				<div class="item-title text-ellipsis">
            					<a href="{{ route('nhac.detail', ['slug' => str_slug($music->song_name), 'id' => $music->id]) }}">{{ $music->song_name }}</a>
            				</div>

            				<div class="item-meta text-sm text-muted">
            		          <span class="item-meta-right">3:10</span>
            		        </div>
            
            
            			</div>
            		</div>
            	</div>
			  @endforeach
          </div>
          <h5 class="m-b">From {{ $artist->art_name }}</h5>
          <div class="row m-b">
			  <div class="col-xs-6 col-sm-6 col-md-3">
				  <div class="item r" data-id="item-{{ $objMusic->id }}" data-src="{{ $objMusic->source }}">
					  <div class="item-media ">
						  <a href="{{ route('nhac.detail', ['slug' => str_slug($objMusic->song_name), 'id' => $objMusic->id]) }}" class="item-media-content" style="background-image: url('{{ asset('')}}/images/b11.jpg');"></a>
						  <div class="item-overlay center">
							  <button  class="btn-playpause">Play</button>
						  </div>
					  </div>
					  <div class="item-info">
						  <div class="item-overlay bottom text-right">
							  <a href="#" class="btn-favorite"><i class="fa fa-heart-o"></i></a>
							  <a href="#" class="btn-more" data-toggle="dropdown"><i class="fa fa-ellipsis-h"></i></a>
							  <div class="dropdown-menu pull-right black lt"></div>
						  </div>
						  <div class="item-title text-ellipsis">
							  <a href="{{ route('nhac.detail', ['slug' => str_slug($objMusic->song_name), 'id' => $objMusic->id]) }}">
								  {{ $objMusic->song_name }}</a>
						  </div>
						  <div class="item-author text-sm text-ellipsis hide">
							  <a href="#" class="text-muted">
								  @if($objMusic->artist_name == '')
									  @php
										  $objArts = $objMusic->artists($objMusic->id);
									  @endphp
									  @foreach($objArts as $key => $artist)
										  @if(!empty($objArts[$key+1]))
											  {{$artist->art_name}} ft.
										  @else
											  {{$artist->art_name}}
										  @endif
									  @endforeach
								  @else
									  {{ $objMusic->artist_name }}
								  @endif
							  </a>
						  </div>
						  <div class="item-meta text-sm text-muted">
            		          <span class="item-meta-stats text-xs ">
            		          	<i class="fa fa-play text-muted"></i> {{ $objMusic->count_listen }}
            		          	<i class="fa fa-heart m-l-sm text-muted"></i> 240
            		          </span>
						  </div>


					  </div>
				  </div>
			  </div>
			  @foreach($objRelateMusic as $music)
                <div class="col-xs-6 col-sm-6 col-md-3">
                	<div class="item r" data-id="item-{{ $music->id }}" data-src="{{ $music->source }}">
            			<div class="item-media ">
            				<a href="{{ route('nhac.detail', ['slug' => str_slug($music->song_name), 'id' => $music->id]) }}" class="item-media-content" style="background-image: url('{{ asset('')}}/images/b11.jpg');"></a>
            				<div class="item-overlay center">
            					<button  class="btn-playpause">Play</button>
            				</div>
            			</div>
            			<div class="item-info">
            				<div class="item-overlay bottom text-right">
            					<a href="#" class="btn-favorite"><i class="fa fa-heart-o"></i></a>
            					<a href="#" class="btn-more" data-toggle="dropdown"><i class="fa fa-ellipsis-h"></i></a>
            					<div class="dropdown-menu pull-right black lt"></div>
            				</div>
            				<div class="item-title text-ellipsis">
            					<a href="{{ route('nhac.detail', ['slug' => str_slug($music->song_name), 'id' => $music->id]) }}">
									{{ $music->song_name }}</a>
            				</div>
            				<div class="item-author text-sm text-ellipsis hide">
            					<a href="#" class="text-muted">
									@if($music->artist_name == '')
										@php
											$objArts = $music->artists($music->id);
										@endphp
										@foreach($objArts as $key => $artist)
											@if(!empty($objArts[$key+1]))
												{{$artist->art_name}} ft.
											@else
												{{$artist->art_name}}
											@endif
										@endforeach
									@else
										{{ $music->artist_name }}
									@endif
								</a>
            				</div>
            				<div class="item-meta text-sm text-muted">
            		          <span class="item-meta-stats text-xs ">
            		          	<i class="fa fa-play text-muted"></i> $music->count_listen
            		          	<i class="fa fa-heart m-l-sm text-muted"></i> 240
            		          </span>
            		        </div>
            
            
            			</div>
            		</div>
            	</div>
			  @endforeach
          </div>
          <h5 class="m-b">Bình luận</h5>
          <div class="streamline m-b m-l">
			  @foreach($objCmt as $cmtItem)
                  <div class="sl-item">
                    <div class="sl-left">
                      <img src="{{ asset('') }}/images/a2.jpg" alt="." class="img-circle">
                    </div>
                    <div class="sl-content">
                      <div class="sl-author m-b-0">
                        <a href="#">{{ $cmtItem->user_id }}</a>
                        <span class="sl-date text-muted">{{ $cmtItem->created_at }}</span>
                      </div>
                      <div>
                        <p>{{ $cmtItem->content }}</p>
                      </div>
                    </div>
                  </div>
				  @endforeach
          </div>
          <h5 class="m-t-lg m-b">Bình luận</h5>
          <form action="{{ route('nhac.comment', ['id' => $objMusic->id]) }}" method="POST">
			  {{ csrf_field() }}
            <div class="form-group row">
              <div class="col-sm-6">
                <label>Họ tên</label>
                <input type="text" class="form-control" name="user_id" placeholder="Họ tên">
              </div>
              <div class="col-sm-6">
                <label >Email</label>
                <input type="email" class="form-control" placeholder="Nhập email">
              </div>
            </div>
            <div class="form-group">
              <label>Bình luận</label>
              <textarea class="form-control" rows="5" name="contents" placeholder="Type your comment"></textarea>
            </div>
            <div class="form-group">
              <button type="submit" class="btn btn-sm white rounded">Bình luận</button>
            </div>
          </form>

      </div>
    </div>




@stop
