@extends('templates.musicplus.master')
@section('content')
    <div class="col-lg-9 b-r no-border-md">
        <div class="padding">


            <div class="page-title m-b">
                <h1 class="inline m-a-0">Chọn thể loại</h1>
                <div class="dropdown inline">
                    <button class="btn btn-sm no-bg h4 m-y-0 v-b dropdown-toggle text-primary" data-toggle="dropdown">
                        Tất cả
                    </button>
                    <div class="dropdown-menu">
                        @php
                            $selectedCat = (!empty($selectedCat))?$selectedCat->id:'';
                        @endphp
                        <a href="{{ route('nhac.catAll') }}" class="dropdown-item @if($selectedCat == '') active @endif">
                            Tất cả
                        </a>
                        @foreach($objCat as $catItem)
                            <a href="{{ route('nhac.cat', ['slug' => str_slug($catItem->name), 'id' => $catItem->id]) }}"
                               class="dropdown-item @if($selectedCat == $catItem->id) active @endif">
                                {{ $catItem->name }}
                            </a>
                        @endforeach
                    </div>
                </div>
            </div>
            <div data-ui-jp="jscroll" class="jscroll-loading-center" data-ui-options="{
            autoTrigger: true,
            loadingHtml: '<i class=\'fa fa-refresh fa-spin text-md text-muted\'></i>',
            padding: 50,
            nextSelector: 'a.jscroll-next:last'
          }">
                <div class="row">
                    @foreach($objMusic as $music)
                        <div class="col-xs-4 col-sm-4 col-md-3">
                            <div class="item r" data-id="item-{{ $music->id }}"
                                 data-src="{{ $music->source }}">
                                <div class="item-media ">
                                    <a href="{{ route('nhac.detail', ['slug' => str_slug($music->song_name), 'id' => $music->id]) }}" class="item-media-content"
                                       style="background-image: url('{{ asset('') }}images/b2.jpg');"></a>
                                    <div class="item-overlay center">
                                        <button class="btn-playpause">Play</button>
                                    </div>
                                </div>
                                <div class="item-info">
                                    <div class="item-overlay bottom text-right">
                                        <a href="#" class="btn-favorite"><i class="fa fa-heart-o"></i></a>
                                        <a href="#" class="btn-more" data-toggle="dropdown"><i
                                                    class="fa fa-ellipsis-h"></i></a>
                                        <div class="dropdown-menu pull-right black lt"></div>
                                    </div>
                                    <div class="item-title text-ellipsis">
                                        <a href="{{ route('nhac.detail', ['slug' => str_slug($music->song_name), 'id' => $music->id]) }}">{{ $music->song_name }}</a>
                                    </div>
                                    <div class="item-author text-sm text-ellipsis ">
                                        <a href="#" class="text-muted">
                                            @if($music->artist_name == '')
                                                @php
                                                    $objArts = $music->artists($music->id);
                                                @endphp
                                                @foreach($objArts as $key => $artist)
                                                    @if(!empty($objArts[$key+1]))
                                                        {{$artist->art_name}} ft.
                                                    @else
                                                        {{$artist->art_name}}
                                                    @endif
                                                @endforeach
                                            @else
                                                {{ $music->artist_name }}
                                            @endif
                                        </a>
                                    </div>


                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>

        </div>
    </div>
@stop