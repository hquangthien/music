<!-- ############ SEARCH START -->
<div class="modal white lt fade" id="search-modal" data-backdrop="false">
    <a data-dismiss="modal" class="text-muted text-lg p-x modal-close-btn">&times;</a>
    <div class="row-col">
        <div class="p-a-lg h-v row-cell v-m">
            <div class="row">
                <div class="col-md-8 offset-md-2">
                    <form action="javascipt:void(0)" class="m-b-md">
                        <div class="input-group input-group-lg">
                            <input type="text" class="form-control" placeholder="Type keyword"
                                   data-ui-toggle-class="hide" data-ui-target="#search-result">
                            <span class="input-group-btn">
                    <button class="btn b-a no-shadow white" type="submit">Search</button>
                  </span>
                        </div>
                    </form>
                    <div id="search-result" class="animated fadeIn">
                        <p class="m-b-md"><strong>23</strong> <span
                                    class="text-muted">Results found for: </span><strong>Keyword</strong></p>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="row item-list item-list-sm item-list-by m-b">
                                    <div class="col-xs-12">
                                        <div class="item r" data-id="item-7"
                                             data-src="http://api.soundcloud.com/tracks/245566366/stream?client_id=a10d44d431ad52868f1bce6d36f5234c">
                                            <div class="item-media ">
                                                <a href="track.detail.html" class="item-media-content"
                                                   style="background-image: url('{{asset('')}}/images/b6.jpg');"></a>
                                            </div>
                                            <div class="item-info">
                                                <div class="item-title text-ellipsis">
                                                    <a href="track.detail.html">Reflection (Deluxe)</a>
                                                </div>
                                                <div class="item-author text-sm text-ellipsis ">
                                                    <a href="artist.detail.html" class="text-muted">Fifth Harmony</a>
                                                </div>
                                                <div class="item-meta text-sm text-muted">
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="item r" data-id="item-3"
                                             data-src="http://api.soundcloud.com/tracks/79031167/stream?client_id=a10d44d431ad52868f1bce6d36f5234c">
                                            <div class="item-media ">
                                                <a href="track.detail.html" class="item-media-content"
                                                   style="background-image: url('{{asset('')}}/images/b2.jpg');"></a>
                                            </div>
                                            <div class="item-info">
                                                <div class="item-title text-ellipsis">
                                                    <a href="track.detail.html">I Wanna Be In the Cavalry</a>
                                                </div>
                                                <div class="item-author text-sm text-ellipsis ">
                                                    <a href="artist.detail.html" class="text-muted">Jeremy Scott</a>
                                                </div>
                                                <div class="item-meta text-sm text-muted">
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="item r" data-id="item-5"
                                             data-src="http://streaming.radionomy.com/JamendoLounge">
                                            <div class="item-media ">
                                                <a href="track.detail.html" class="item-media-content"
                                                   style="background-image: url('{{asset('')}}/images/b4.jpg');"></a>
                                            </div>
                                            <div class="item-info">
                                                <div class="item-title text-ellipsis">
                                                    <a href="track.detail.html">Live Radio</a>
                                                </div>
                                                <div class="item-author text-sm text-ellipsis ">
                                                    <a href="artist.detail.html" class="text-muted">Radionomy</a>
                                                </div>
                                                <div class="item-meta text-sm text-muted">
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="item r" data-id="item-1"
                                             data-src="http://api.soundcloud.com/tracks/269944843/stream?client_id=a10d44d431ad52868f1bce6d36f5234c">
                                            <div class="item-media ">
                                                <a href="track.detail.html" class="item-media-content"
                                                   style="background-image: url('{{asset('')}}/images/b1.jpg');"></a>
                                            </div>
                                            <div class="item-info">
                                                <div class="item-title text-ellipsis">
                                                    <a href="track.detail.html">Pull Up</a>
                                                </div>
                                                <div class="item-author text-sm text-ellipsis ">
                                                    <a href="artist.detail.html" class="text-muted">Summerella</a>
                                                </div>
                                                <div class="item-meta text-sm text-muted">
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="row item-list item-list-sm item-list-by m-b">
                                    <div class="col-xs-12">
                                        <div class="item">
                                            <div class="item-media rounded ">
                                                <a href="artist.detail.html" class="item-media-content"
                                                   style="background-image: url('{{asset('')}}/images/a7.jpg');"></a>
                                            </div>
                                            <div class="item-info ">
                                                <div class="item-title text-ellipsis">
                                                    <a href="artist.detail.html">Richard Carr</a>
                                                    <div class="text-sm text-muted">6 songs</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="item">
                                            <div class="item-media rounded ">
                                                <a href="artist.detail.html" class="item-media-content"
                                                   style="background-image: url('{{asset('')}}/images/a8.jpg');"></a>
                                            </div>
                                            <div class="item-info ">
                                                <div class="item-title text-ellipsis">
                                                    <a href="artist.detail.html">Sara King</a>
                                                    <div class="text-sm text-muted">14 songs</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="item">
                                            <div class="item-media rounded ">
                                                <a href="artist.detail.html" class="item-media-content"
                                                   style="background-image: url('{{asset('')}}/images/b1.jpg');"></a>
                                            </div>
                                            <div class="item-info ">
                                                <div class="item-title text-ellipsis">
                                                    <a href="artist.detail.html">Melissa Garza</a>
                                                    <div class="text-sm text-muted">20 songs</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="item">
                                            <div class="item-media rounded ">
                                                <a href="artist.detail.html" class="item-media-content"
                                                   style="background-image: url('{{asset('')}}/images/a1.jpg');"></a>
                                            </div>
                                            <div class="item-info ">
                                                <div class="item-title text-ellipsis">
                                                    <a href="artist.detail.html">James Garcia</a>
                                                    <div class="text-sm text-muted">9 songs</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="top-search" class="btn-groups">
                        <strong class="text-muted">Top searches: </strong>
                        <a href="#" class="btn btn-xs white">Happy</a>
                        <a href="#" class="btn btn-xs white">Music</a>
                        <a href="#" class="btn btn-xs white">Weekend</a>
                        <a href="#" class="btn btn-xs white">Summer</a>
                        <a href="#" class="btn btn-xs white">Holiday</a>
                        <a href="#" class="btn btn-xs white">Blue</a>
                        <a href="#" class="btn btn-xs white">Soul</a>
                        <a href="#" class="btn btn-xs white">Calm</a>
                        <a href="#" class="btn btn-xs white">Nice</a>
                        <a href="#" class="btn btn-xs white">Home</a>
                        <a href="#" class="btn btn-xs white">SLeep</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ############ SEARCH END -->
<!-- ############ SHARE START -->
<div id="share-modal" class="modal fade animate">
    <div class="modal-dialog">
        <div class="modal-content fade-down">
            <div class="modal-header">

                <h5 class="modal-title">Share</h5>
            </div>
            <div class="modal-body p-lg">
                <div id="share-list" class="m-b">
                    <a href="" class="btn btn-icon btn-social rounded btn-social-colored indigo" title="Facebook">
                        <i class="fa fa-facebook"></i>
                        <i class="fa fa-facebook"></i>
                    </a>

                    <a href="" class="btn btn-icon btn-social rounded btn-social-colored light-blue" title="Twitter">
                        <i class="fa fa-twitter"></i>
                        <i class="fa fa-twitter"></i>
                    </a>

                    <a href="" class="btn btn-icon btn-social rounded btn-social-colored red-600" title="Google+">
                        <i class="fa fa-google-plus"></i>
                        <i class="fa fa-google-plus"></i>
                    </a>

                    <a href="" class="btn btn-icon btn-social rounded btn-social-colored blue-grey-600" title="Trumblr">
                        <i class="fa fa-tumblr"></i>
                        <i class="fa fa-tumblr"></i>
                    </a>

                    <a href="" class="btn btn-icon btn-social rounded btn-social-colored red-700" title="Pinterst">
                        <i class="fa fa-pinterest"></i>
                        <i class="fa fa-pinterest"></i>
                    </a>
                </div>
                <div>
                    <input class="form-control" value="http://plamusic.com/slug"/>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ############ SHARE END -->

<!-- ############ LAYOUT END-->
</div>
<!-- build:js scripts/app.min.js -->
<!-- Bootstrap -->
<script src="{{asset('')}}/libs/tether/dist/js/tether.min.js"></script>
<script src="{{asset('')}}/libs/bootstrap/dist/js/bootstrap.js"></script>
<!-- core -->
<script src="{{asset('')}}/libs/jQuery-Storage-API/jquery.storageapi.min.js"></script>
<script src="{{asset('')}}/libs/jquery.stellar/jquery.stellar.min.js"></script>
<script src="{{asset('')}}/libs/owl.carousel/dist/owl.carousel.min.js"></script>
<script src="{{asset('')}}/libs/jscroll/jquery.jscroll.min.js"></script>
<script src="{{asset('')}}/libs/PACE/pace.min.js"></script>
<script src="{{asset('')}}/libs/screenfull/dist/screenfull.min.js"></script>
<script src="{{asset('')}}/libs/mediaelement/build/mediaelement-and-player.min.js"></script>
<script src="{{asset('')}}/libs/mediaelement/build/mep.js"></script>
<script src="{{asset('')}}/scripts/player.js"></script>

<script src="{{asset('')}}/scripts/config.lazyload.js"></script>
<script src="{{asset('')}}/scripts/ui-load.js"></script>
<script src="{{asset('')}}/scripts/ui-jp.js"></script>
<script src="{{asset('')}}/scripts/ui-include.js"></script>
<script src="{{asset('')}}/scripts/ui-device.js"></script>
<script src="{{asset('')}}/scripts/ui-form.js"></script>
<script src="{{asset('')}}/scripts/ui-nav.js"></script>
<script src="{{asset('')}}/scripts/ui-screenfull.js"></script>
<script src="{{asset('')}}/scripts/ui-scroll-to.js"></script>
<script src="{{asset('')}}/scripts/ui-toggle-class.js"></script>
<script src="{{asset('')}}/scripts/ui-taburl.js"></script>
<script src="{{asset('')}}/scripts/app.js"></script>
<script src="{{asset('')}}/scripts/site.js"></script>
@yield('js')
<!-- endbuild -->
</body>
</html>
