@include('templates.musicplus.header')
<div class="page-content" id="pjax-container">
@yield('slide')


 <div class="row-col">
@yield('content')



@include('templates.musicplus.right-bar')
@include('templates.musicplus.footer')
