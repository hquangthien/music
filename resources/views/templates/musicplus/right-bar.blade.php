<div class="col-lg-3 w-xxl w-auto-md">
    <div class="padding" style="bottom: 60px;" data-ui-jp="stick_in_parent">
        <h6 class="text text-muted">Nghe nhiều nhất</h6>
        <div class="row item-list item-list-sm m-b">
            @foreach($rightBarMusic as $key => $rightMusic)
                <div class="col-xs-12">
                    <div class="item r" data-id="item-{{ $rightMusic->id }}"
                         data-src="{{ $rightMusic->source }}">
                        <div class="item-media ">
                            <a href="{{ route('nhac.detail', ['slug' => str_slug($rightMusic->song_name), 'id' => $rightMusic->id]) }}" class="item-media-content"
                               style="background-image: url('{{ asset('') }}/images/b7.jpg');"></a>
                        </div>
                        <div class="item-info">
                            <div class="item-title text-ellipsis">
                                <a href="{{ route('nhac.detail', ['slug' => str_slug($rightMusic->song_name), 'id' => $rightMusic->id]) }}">
                                    {{ $rightMusic->song_name }}</a>
                            </div>
                            <div class="item-author text-sm text-ellipsis ">
                                <a href="#" class="text-muted">
                                    @if($rightMusic->artist_name == '')
                                        @php
                                            $objArts = $rightMusic->artists($rightMusic->id);
                                        @endphp
                                        @foreach($objArts as $key => $artist)
                                            @if(!empty($objArts[$key+1]))
                                                {{$artist->art_name}} ft.
                                            @else
                                                {{$artist->art_name}}
                                            @endif
                                        @endforeach
                                    @else
                                        {{ $rightMusic->artist_name }}
                                    @endif
                                </a>
                            </div>


                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <h6 class="text text-muted">Go mobile</h6>
        <div class="btn-groups">
            <a href="" class="btn btn-sm dark lt m-r-xs" style="width: 135px">
                <span class="pull-left m-r-sm">
                  <i class="fa fa-apple fa-2x"></i>
                </span>
                <span class="clear text-left l-h-1x">
                  <span class="text-muted text-xxs">Download on the</span>
                  <b class="block m-b-xs">App Store</b>
                </span>
            </a>
            <a href="" class="btn btn-sm dark lt" style="width: 133px">
                <span class="pull-left m-r-sm">
                  <i class="fa fa-play fa-2x"></i>
                </span>
                <span class="clear text-left l-h-1x">
                  <span class="text-muted text-xxs">Get it on</span>
                  <b class="block m-b-xs m-r-xs">Google Play</b>
                </span>
            </a>
        </div>
        <div class="b-b m-y"></div>
        <div class="nav text-sm _600">
            <a href="#" class="nav-link text-muted m-r-xs">About</a>
            <a href="#" class="nav-link text-muted m-r-xs">Contact</a>
            <a href="#" class="nav-link text-muted m-r-xs">Legal</a>
            <a href="#" class="nav-link text-muted m-r-xs">Policy</a>
        </div>
        <p class="text-muted text-xs p-b-lg">&copy; Copyright 2016</p>
    </div>
</div>
</div>
</div>

<!-- ############ PAGE END-->

</div>
</div>
<!-- / -->

<!-- ############ SWITHCHER START-->
<div id="switcher">
    <div class="switcher white" id="sw-theme">
        <a href="#" data-ui-toggle-class="active" data-ui-target="#sw-theme" class="white sw-btn">
            <i class="fa fa-gear text-muted"></i>
        </a>
        <div class="box-header">
            <strong>Theme Switcher</strong>
        </div>
        <div class="box-divider"></div>
        <div class="box-body">
            <p id="settingLayout" class="hidden-md-down">
                <label class="md-check m-y-xs" data-target="container">
                    <input type="checkbox">
                    <i class="green"></i>
                    <span>Boxed Layout</span>
                </label>
                <label class="m-y-xs pointer" data-ui-fullscreen data-target="fullscreen">
                    <span class="fa fa-expand fa-fw m-r-xs"></span>
                    <span>Fullscreen Mode</span>
                </label>
            </p>
            <p>Colors:</p>
            <p data-target="color">
                <label class="radio radio-inline m-a-0 ui-check ui-check-color ui-check-md">
                    <input type="radio" name="color" value="primary">
                    <i class="primary"></i>
                </label>
                <label class="radio radio-inline m-a-0 ui-check ui-check-color ui-check-md">
                    <input type="radio" name="color" value="accent">
                    <i class="accent"></i>
                </label>
                <label class="radio radio-inline m-a-0 ui-check ui-check-color ui-check-md">
                    <input type="radio" name="color" value="warn">
                    <i class="warn"></i>
                </label>
                <label class="radio radio-inline m-a-0 ui-check ui-check-color ui-check-md">
                    <input type="radio" name="color" value="success">
                    <i class="success"></i>
                </label>
                <label class="radio radio-inline m-a-0 ui-check ui-check-color ui-check-md">
                    <input type="radio" name="color" value="info">
                    <i class="info"></i>
                </label>
                <label class="radio radio-inline m-a-0 ui-check ui-check-color ui-check-md">
                    <input type="radio" name="color" value="blue">
                    <i class="blue"></i>
                </label>
                <label class="radio radio-inline m-a-0 ui-check ui-check-color ui-check-md">
                    <input type="radio" name="color" value="warning">
                    <i class="warning"></i>
                </label>
                <label class="radio radio-inline m-a-0 ui-check ui-check-color ui-check-md">
                    <input type="radio" name="color" value="danger">
                    <i class="danger"></i>
                </label>
            </p>
            <p>Themes:</p>
            <div data-target="bg" class="text-u-c text-center _600 clearfix">
                <label class="p-a col-xs-3 light pointer m-a-0">
                    <input type="radio" name="theme" value="" hidden>
                    <i class="active-checked fa fa-check"></i>
                </label>
                <label class="p-a col-xs-3 grey pointer m-a-0">
                    <input type="radio" name="theme" value="grey" hidden>
                    <i class="active-checked fa fa-check"></i>
                </label>
                <label class="p-a col-xs-3 dark pointer m-a-0">
                    <input type="radio" name="theme" value="dark" hidden>
                    <i class="active-checked fa fa-check"></i>
                </label>
                <label class="p-a col-xs-3 black pointer m-a-0">
                    <input type="radio" name="theme" value="black" hidden>
                    <i class="active-checked fa fa-check"></i>
                </label>
            </div>
        </div>
    </div>
</div>
<!-- ############ SWITHCHER END-->